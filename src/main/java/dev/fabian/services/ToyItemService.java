package dev.fabian.services;

import dev.fabian.data.ToyItemDao;
import dev.fabian.data.ToyItemDaoImpl;
import dev.fabian.models.ToyItem;
import io.javalin.http.BadRequestResponse;


import java.util.List;


public class ToyItemService {
    private final ToyItemDao toyItemDao = new ToyItemDaoImpl();

    public List<ToyItem> getAll(){

        return toyItemDao.getAllItems();
    }

    public ToyItem getById(int id){

        return toyItemDao.getItemById(id);
    }

    public ToyItem add(ToyItem item){

        return toyItemDao.addNewItem(item);
    }

    public void delete(int id){

        toyItemDao.deleteItem(id);
    }

    public void put(int id, String toyName){

        toyItemDao.putItem(id, toyName);
    }

    public List<ToyItem> getItemsInRange(String minPrice, String maxPrice) {
        if (minPrice != null) {
            if (matchesPriceFormat(minPrice)) {
                if (maxPrice != null) {
                    if (matchesPriceFormat(maxPrice)) {
                        return toyItemDao.getItemsInRange(Double.parseDouble(minPrice), Double.parseDouble(maxPrice));
                    }
                    throw new BadRequestResponse("improper price format provided for max-price");
                }
                return toyItemDao.getItemsWithMinPrice(Double.parseDouble(minPrice));
            }
            throw new BadRequestResponse("improper price format provided for min-price");
        } else {
            if (matchesPriceFormat(maxPrice)) {
                return toyItemDao.getItemsWithMaxPrice(Double.parseDouble(maxPrice));
            }
            throw new BadRequestResponse("improper price format provided for max-price");
        }
    }

    private boolean matchesPriceFormat(String str){
        return str.matches("\\d{0,4}(\\.\\d{1,2})?");
    }

}
