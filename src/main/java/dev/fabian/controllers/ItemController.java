package dev.fabian.controllers;

import dev.fabian.models.ToyItem;
import dev.fabian.services.ToyItemService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 *
 * This class is used by our application
 * to  be able to handle get(),post(),delete(),put(),
 * http methods to interact with an external database
 *
 */

public class ItemController {
    private final Logger logger = LoggerFactory.getLogger(ItemController.class);
    private ToyItemService service = new ToyItemService();

    /*
     *
     * This method is used by our application
     * to handle an http get() request
     * to retrieve toy_items table in the database
     *
     */
    public void handleGetItemsRequest(Context ctx){
        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        if(maxPrice!=null || minPrice!=null){
            logger.info("getting items in price range");
            ctx.json(service.getItemsInRange(minPrice, maxPrice));
        } else { // /items with no query params
            logger.info("getting all items");
            ctx.json(service.getAll());
        }
    }
    /*
     *
     * This method is used by our application
     * to be able to handle http get() request
     * and retrieve item with a given id
     *
     */
    public void handleGetItemByIdRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            ToyItem item = service.getById(idInput);
            if (item == null) {
                logger.warn("no item present with id: {}", idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("getting item with id: {}", idInput);
                ctx.json(item);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }
    /*
     *
     * This method is used by our application
     * to be able to handle http post() method
     * to add a new record in toy_items table
     *
     */
    public void handlePostNewItem(Context ctx){
        ToyItem item = ctx.bodyAsClass(ToyItem.class);
        logger.info("adding new item: {}", item);
        service.add(item);
        ctx.status(201);
        //ctx.json(newItem);
        ctx.json(item);
    }
    /*
     *
     * This method is used by our application
     * to use http delete() to a particular record from
     * toy_items table with a given id
     *
     */
    public void handleDeleteById(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: {}", idInput);
            service.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }
    /*
     *
     * This method is used by our application
     * to be able to use http put() method on an
     * existing record in toy_items with a given id
     *
     */
    public void handlePutById(Context ctx){
        String idString = ctx.pathParam("id");
        String toyName =  ctx.queryParam("name");

        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("updating record with id: {}", idString);
            service.put(idInput, toyName);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }

}
