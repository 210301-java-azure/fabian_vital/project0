package dev.fabian.controllers;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 *
 * This class authenticates a user to allow
 * data access through the application to
 * an external database
 *
 */
public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    public void authenticateLogin(Context ctx){
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");
        logger.info("{} attempted login", user);
        if(user!=null && user.equals("fabian")){
            if(pass!=null && pass.equals("securepassword")){
                logger.info("successful login");
                // send back token
                ctx.header("Authorization", "admin-auth-token");
                ctx.status(200);
                return;
            }
            throw new UnauthorizedResponse("Incorrect password");
        }

        throw new UnauthorizedResponse("Username not recognized");
    }

    public void authorizeToken(Context ctx){
        logger.info("attempting to authorize token");

        String authHeader = ctx.header("Authorization");

        if(authHeader!=null && authHeader.equals("admin-auth-token")){
            logger.info("request is authorized, proceeding to handler method");
        } else {
            logger.warn("improper authorization");
            throw new UnauthorizedResponse();
        }
    }

}
