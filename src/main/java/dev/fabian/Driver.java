package dev.fabian;

/*
 *
 * This class runs our local server to
 * enable any external client to connect to this server.
 */
public class Driver {
    public static void main(String[] args){
        JavalinApp javalinApp = new JavalinApp();
        javalinApp.start(7000);
    }


}
