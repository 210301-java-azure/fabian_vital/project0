package dev.fabian;

import dev.fabian.controllers.AuthController;
import dev.fabian.controllers.ItemController;

import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.post;

/*
 *
 * This class makes it possible for
 * our application to handle our http request/responses
 * using Javalin web framework
 *
 */
public class JavalinApp {
    ItemController itemController = new ItemController();
    AuthController authController = new AuthController();

    Javalin app = Javalin.create().routes(()->{

        path("items", ()->{
            before("/",authController::authorizeToken);
            get(itemController::handleGetItemsRequest);
            post(itemController::handlePostNewItem);
            //
            path(":id",()->{
                before("/", authController::authorizeToken);
                get(itemController::handleGetItemByIdRequest);
                delete(itemController::handleDeleteById);
                put(itemController::handlePutById);
            });
        });


        path("login",()->
                post(authController::authenticateLogin)
        );
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }


}
