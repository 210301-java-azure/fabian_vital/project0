package dev.fabian.data;

import dev.fabian.models.Child;

import java.util.List;

/*
 *
 * Interface used as our Data Access Object
 * to communicate with an external database
 * using a child object
 *
 */
public interface ChildDao {
    List<Child> getAllChildren();
    Child getChildById(int id);
    List<Child> getChildInAgeRange(int max, int min);
    List<Child> getChildWithMaxAge(int max);
    List<Child> getChildWithMinAge(int min);
    Child addNewChild(Child child);
    void deleteChild(int id);
    void putChild(int id, String childName);
}
