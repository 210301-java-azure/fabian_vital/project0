package dev.fabian.data;

import dev.fabian.models.Child;
import dev.fabian.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/*
 *
 * This class implements all the methods in
 * the ChildDao interface to send queries to
 * an external database and query CRUD commands
 *
 */
public class ChildDaoImpl implements ChildDao {
    private final Logger logger = LoggerFactory.getLogger(ChildDaoImpl.class);

    @Override
    public List<Child> getAllChildren(){
        List<Child> children = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery("select * from children");

            while(resultSet.next()){
                int id = resultSet.getInt(ChildDaoImpl.ChildMapping.ID);
                String name = resultSet.getString(ChildDaoImpl.ChildMapping.NAME);
                int age = resultSet.getInt(ChildDaoImpl.ChildMapping.AGE);
                String gender = resultSet.getString(ChildDaoImpl.ChildMapping.GENDER);
                int favoriteToy = resultSet.getInt(ChildDaoImpl.ChildMapping.FAVORITETOY);
                Child child = new Child(id,name,age,gender,favoriteToy);
                children.add(child);
            }
            logger.info("selecting all children from the database - {} children retrieved", children.size());
        } catch (SQLException e) {
            logException(e);
        }
        return children;

    }

    @Override
    public Child getChildById(int id) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from children where childid = ?")) {
            prepareStatement.setInt(1,id);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                String name = resultSet.getString(ChildDaoImpl.ChildMapping.NAME);
                int age = resultSet.getInt(ChildDaoImpl.ChildMapping.AGE);
                String gender= resultSet.getString(ChildDaoImpl.ChildMapping.GENDER);
                int favoriteToy= resultSet.getInt(ChildDaoImpl.ChildMapping.FAVORITETOY);


                logger.info("1 child retrieved from database with the id: {}", id);
                return new Child(id, name, age, gender, favoriteToy);
            }
        } catch (SQLException e) {
            logException(e);
        }
        return null;
    }

    @Override
    public List<Child> getChildInAgeRange(int min, int max) {
        List<Child> children = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from children where age > ? and age < ?")) {
            prepareStatement.setInt(1,min);
            prepareStatement.setInt(2,max);
            ResultSet resultSet = prepareStatement.executeQuery();

            while(resultSet.next()){
                int id = resultSet.getInt(ChildDaoImpl.ChildMapping.ID);
                String name = resultSet.getString(ChildDaoImpl.ChildMapping.NAME);
                int age = resultSet.getInt(ChildDaoImpl.ChildMapping.AGE);
                String gender = resultSet.getString(ChildDaoImpl.ChildMapping.GENDER);
                int favoriteToy = resultSet.getInt(ChildDaoImpl.ChildMapping.FAVORITETOY);

                children.add(new Child(id,name,age, gender, favoriteToy));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return children;
    }

    @Override
    public List<Child> getChildWithMaxAge(int max) {
        List<Child> child = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from children where age < ?")) {
            prepareStatement.setInt(1,max);

            ResultSet resultSet = prepareStatement.executeQuery();

            while(resultSet.next()){
                int id = resultSet.getInt(ChildDaoImpl.ChildMapping.ID);
                String name = resultSet.getString(ChildDaoImpl.ChildMapping.NAME);
                int age= resultSet.getInt(ChildDaoImpl.ChildMapping.AGE);
                String gender = resultSet.getString(ChildDaoImpl.ChildMapping.GENDER);
                int favoriteToy= resultSet.getInt(ChildDaoImpl.ChildMapping.FAVORITETOY);
                child.add(new Child(id, name, age, gender, favoriteToy));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return child;
    }

    @Override
    public List<Child> getChildWithMinAge(int min) {
        List<Child> children = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from children where age > ?")) {
            prepareStatement.setDouble(1,min);
            ResultSet resultSet = prepareStatement.executeQuery();
            while(resultSet.next()){
                int id = resultSet.getInt(ChildDaoImpl.ChildMapping.ID);
                String name = resultSet.getString(ChildDaoImpl.ChildMapping.NAME);
                int age = resultSet.getInt(ChildDaoImpl.ChildMapping.AGE);
                String gender = resultSet.getString(ChildDaoImpl.ChildMapping.GENDER);
                int favoriteToy= resultSet.getInt(ChildDaoImpl.ChildMapping.AGE);
                children.add(new Child(id, name, age, gender, favoriteToy));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return children;
    }
    @Override
    public Child addNewChild(Child child) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into children values (default,?,?,?,?)")){
            preparedStatement.setString(1, child.getChildName());
            preparedStatement.setInt(2, child.getAge());
            preparedStatement.setString(3, child.getGender());
            preparedStatement.setInt(4, child.getFavoriteToy());

            preparedStatement.executeUpdate();
            logger.info("successfully added new toy item with id: {}", child.getChildID());

        } catch (SQLException e) {
            logException(e);
        }
        return child;
    }

    @Override
    public void deleteChild(int id) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("delete from children where childid = ?")){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.info("deleted the child with id: {}", id);
        } catch (SQLException e) {
            logException(e);
        }
    }

    @Override
    public void putChild(int id, String childName) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("update children set childname = ? where childid = ?")){
            preparedStatement.setString(1, childName);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            logger.info("updated the toy item with id: {}", id);
        } catch (SQLException e) {
            logException(e);
        }
    }

    public void logException(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }

    private static class ChildMapping {
        private static final String ID = "childID";
        private static final String NAME = "childName";
        private static final String AGE = "age";
        private static final String GENDER = "gender";
        private static final String FAVORITETOY = "favoriteToy";


    }
}

