package dev.fabian.data;

import dev.fabian.models.ToyItem;

import java.util.List;

/*
*
* Interface used as our Data Access Object
* to communicate with an external database
* using a toy item object
*
 */
public interface ToyItemDao {
    List<ToyItem> getAllItems();
    ToyItem getItemById(int id);
    List<ToyItem> getItemsInRange(double max, double min);
    List<ToyItem> getItemsWithMaxPrice(double max);
    List<ToyItem> getItemsWithMinPrice(double min);
    ToyItem addNewItem(ToyItem item);
    void deleteItem(int id);
    void putItem(int id, String toyName);
}
