package dev.fabian.data;

import dev.fabian.models.ToyItem;

import java.util.ArrayList;
import java.util.List;

/*
 *
 * This class provides an example
 * of toy_items data and how to retrieve
 * this data from an arraylist.
 *
 */

public class ToyItemData {
    private List<ToyItem> items = new ArrayList<>();

    public ToyItemData(){
        super();
        items.add(new ToyItem(5, "truck", 15.00));
        items.add(new ToyItem(6,"tea set", 10.00));
        items.add(new ToyItem(7, "boat", 25.00));
        items.add(new ToyItem(8, "race car", 5.00));

    }
    public List<ToyItem> getAllItems(){

        return new ArrayList<>(items);
    }
}
