package dev.fabian.data;

import dev.fabian.models.ToyItem;
import dev.fabian.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/*
 *
 * This class implements all the methods in
 * the ToyItemDao interface to send queries to
 * an external database.
 *
 */
public class ToyItemDaoImpl implements ToyItemDao{
    private final Logger logger = LoggerFactory.getLogger(ToyItemDaoImpl.class);

    @Override
    public List<ToyItem> getAllItems() {
        List<ToyItem> items = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery("select * from toy_items");

            while(resultSet.next()){
                int id = resultSet.getInt(ToyItemMapping.TOYID);
                String name = resultSet.getString(ToyItemMapping.ITEM);
                double price = resultSet.getDouble(ToyItemMapping.PRICE);

                ToyItem item = new ToyItem(id,name,price);
                items.add(item);
            }
            logger.info("selecting all toy items from the database - {} items retrieved", items.size());
        } catch (SQLException e) {
            logException(e);
        }
        return items;
    }

    @Override
    public ToyItem getItemById(int id) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from toy_items where toyid = ?")) {
            prepareStatement.setInt(1,id);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                String name = resultSet.getString(ToyItemMapping.ITEM);
                double price = resultSet.getDouble(ToyItemMapping.PRICE);

                logger.info("1 toy item retrieved from database with the id: {}", id);
                return new ToyItem(id, name, price);
            }
        } catch (SQLException e) {
            logException(e);
        }
        return null;
    }

    @Override
    public List<ToyItem> getItemsInRange(double min, double max) {
        List<ToyItem> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from toy_items where price > ? and price < ?")) {
            prepareStatement.setDouble(1,min);
            prepareStatement.setDouble(2,max);
            ResultSet resultSet = prepareStatement.executeQuery();
            while(resultSet.next()){
                int id = resultSet.getInt(ToyItemMapping.TOYID);
                String name = resultSet.getString(ToyItemMapping.ITEM);
                double price = resultSet.getDouble(ToyItemMapping.PRICE);

                items.add(new ToyItem(id,name,price));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return items;
    }

    @Override
    public List<ToyItem> getItemsWithMaxPrice(double max) {
        List<ToyItem> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from toy_items where price < ?")) {
            prepareStatement.setDouble(1,max);
            ResultSet resultSet = prepareStatement.executeQuery();
            while(resultSet.next()){
                int id = resultSet.getInt(ToyItemMapping.TOYID);
                String name = resultSet.getString(ToyItemMapping.ITEM);
                double price = resultSet.getDouble(ToyItemMapping.PRICE);
                items.add(new ToyItem(id, name, price));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return items;
    }

    @Override
    public List<ToyItem> getItemsWithMinPrice(double min) {
        List<ToyItem> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from toy_items where price > ?")) {
            prepareStatement.setDouble(1,min);
            ResultSet resultSet = prepareStatement.executeQuery();
            while(resultSet.next()){
                int id = resultSet.getInt(ToyItemMapping.TOYID);
                String name = resultSet.getString(ToyItemMapping.ITEM);
                double price = resultSet.getDouble(ToyItemMapping.PRICE);
                items.add(new ToyItem(id, name, price));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return items;
    }

    @Override
    public ToyItem addNewItem(ToyItem item) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into toy_items values (default,?,?)")){
            preparedStatement.setString(1, item.getToyName());
            preparedStatement.setDouble(2, item.getPrice());

            preparedStatement.executeUpdate();
            logger.info("successfully added new toy item with id: {}", item.getToyID());

        } catch (SQLException e) {
            logException(e);
        }
        return item;
    }


    @Override
    public void deleteItem(int id) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("delete from toy_items where toyid = ?")){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.info("deleted the toy item with id: {}", id);
        } catch (SQLException e) {
            logException(e);
        }
    }

    public void putItem(int id, String toyName) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("update toy_items set item = ? where toyid = ?")){
            preparedStatement.setString(1, toyName);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            logger.info("updated the toy item with id: {}", id);
        } catch (SQLException e) {
            logException(e);
        }
    }

    public void logException(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }

    private static class ToyItemMapping {
        private static final String TOYID = "toyid";
        private static final String ITEM = "item";
        private static final String PRICE = "price";
    }

}
