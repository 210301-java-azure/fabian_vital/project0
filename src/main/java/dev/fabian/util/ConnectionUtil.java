package dev.fabian.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 *
 * This utility class makes the connection
 * with the external postgreSQL database to allow users to
 * access the database.
 *
 */
public class ConnectionUtil {
    private static Connection connection;

    private ConnectionUtil(){
        super();
    }

    public static Connection getConnection() throws SQLException {
        if(connection == null || connection.isClosed()) {
            String connectionUrl ="jdbc:postgresql://localhost:5432/p0_1?";
            String username = "postgres";
            String password = "Fabian.SQL1";

            connection = DriverManager.getConnection(connectionUrl, username, password);
        }
        return connection;
    }

}
