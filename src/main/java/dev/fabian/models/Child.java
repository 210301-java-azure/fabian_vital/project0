package dev.fabian.models;

import java.util.Objects;

/*
 *
 * This class is used by our application
 * to create a child object for each table record
 *
 */
public class Child {
    private int childID;
    private String childName;
    private int age;
    private String gender;
    private int favoriteToy;

    public Child(){

        super();
    }

    public Child(int id, String name, int age, String gender,int favoriteToy){
        this.childID = id;
        this.childName = name;
        this.age = age;
        this.gender = gender;
        this.favoriteToy = favoriteToy;
    }

    public int getChildID() {
        return childID;
    }

    public String getChildName() {

        return childName;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {

        return gender;
    }

    public void setChildID(int childID) {

        this.childID = childID;
    }

    public void setChildName(String childName) {

        this.childName = childName;
    }

    public void setAge(int age) {

        this.age = age;
    }

    public void setGender(String gender) {

        this.gender = gender;
    }

    public int getFavoriteToy() {

        return favoriteToy;
    }

    public void setFavoriteToy(int favoriteToy) {

        this.favoriteToy = favoriteToy;
    }

    @Override
    public String toString() {
        return "Child{" +
                "childID=" + childID +
                ", childName='" + childName + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Child child = (Child) o;
        return childID == child.childID && age == child.age && Objects.equals(childName, child.childName) && Objects.equals(gender, child.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(childID, childName, age, gender);
    }
}