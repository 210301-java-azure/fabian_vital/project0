package dev.fabian.models;

import java.io.Serializable;
import java.util.Objects;

/*
 *
 * This class is used by our application
 * to create a toy item object for each table record
 *
 */
public class ToyItem implements Serializable {
    private int toyID;
    private String toyName;
    private double price;

    public ToyItem(){
        super();
    }
    public ToyItem(int id, String name, double price){
        this.toyID = id;
        this.toyName = name;
        this.price = price;
    }
    public ToyItem(String name){
        this.toyName = name;
    }

    public int getToyID() {
        return toyID;
    }

    public String getToyName() {
        return toyName;
    }

    public double getPrice() {
        return price;
    }

    public void setToyID(int toyID) {
        this.toyID = toyID;
    }

    public void setToyName(String toyName) {
        this.toyName = toyName;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ToyItem{" +
                "toyID=" + toyID +
                ", toyName='" + toyName + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToyItem toyItem = (ToyItem) o;
        return toyID == toyItem.toyID && Double.compare(toyItem.price, price) == 0 && Objects.equals(toyName, toyItem.toyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(toyID, toyName, price);
    }
}
