import dev.fabian.controllers.ItemController;

import dev.fabian.models.ToyItem;
import dev.fabian.services.ToyItemService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/*
 *
 * This class is utilized to implement some unit tests
 * on our itemController that handles http methods
 *
 */
public class ItemControllerTest {

    @InjectMocks
    private ItemController itemController;

    @Mock
    private ToyItemService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllItemsHandler(){
        Context context = mock(Context.class);

        List<ToyItem> items = new ArrayList<>();
        items.add(new ToyItem(21,"basketball", 3.00));
        items.add(new ToyItem(35,"puzzle", 5.00));
        items.add(new ToyItem(80,"legos box", 30.00));

        when(service.getAll()).thenReturn(items);
        itemController.handleGetItemsRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRangeWithMin(){
        Context context = mock(Context.class);

        List<ToyItem> items = new ArrayList<>();
        items.add(new ToyItem(12,"volleyball", 3.00));
        items.add(new ToyItem(20,"action figure", 15.00));

        when(service.getItemsInRange("20", null)).thenReturn(items);
        when(context.queryParam("min-price")).thenReturn("20");

        itemController.handleGetItemsRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRangeWithMax(){
        Context context = mock(Context.class);

        List<ToyItem> items = new ArrayList<>();
        items.add(new ToyItem(21,"basketball", 3.00));
        items.add(new ToyItem(12,"volleyball", 3.00));

        when(service.getItemsInRange(null, "80")).thenReturn(items);
        when(context.queryParam("max-price")).thenReturn("80");

        itemController.handleGetItemsRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRange(){
        Context context = mock(Context.class);

        List<ToyItem> items = new ArrayList<>();
        items.add(new ToyItem(35,"boat", 35.00));

        when(service.getItemsInRange("20", "50")).thenReturn(items);
        when(context.queryParam("min-price")).thenReturn("20");
        when(context.queryParam("max-price")).thenReturn("50");

        itemController.handleGetItemsRequest(context);
        verify(context).json(items);
    }
//*/
}
